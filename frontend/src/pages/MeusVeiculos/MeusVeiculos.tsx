import { Typography } from "@mui/material";
import { PageContainer } from "./MeusVeiculos.styles";


const MeusVeiculos = () => {
  return (
    <PageContainer>
      <Typography variant="h5" color={"#797979"} fontWeight={400}>
        Meus veículos
      </Typography>
    </PageContainer>
  );
};

export default MeusVeiculos;
