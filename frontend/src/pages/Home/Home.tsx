import MiniDrawer from './components/Drawer';

const Home = () => {
    return (
        <>
        <MiniDrawer />
        </>
    );
};

export default Home;