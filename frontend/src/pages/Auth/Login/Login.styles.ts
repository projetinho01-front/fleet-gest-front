import styled from 'styled-components';
import { TextField, Button, FormControlLabel } from '@mui/material';

export const PageContainer = styled.div`
  display: flex;
  flex-direction: row;
  height: 100vh;
  background: linear-gradient(to bottom right, #3aafb9, #034748, #001021);
`;

export const Container = styled.div`
  display: flex;
  width: 50%;
  align-items: center;
  justify-content: center;
`;

export const LoginContainer = styled.div`
  display: flex;
  margin-left: 50px;
  width: 385px;
  height: 465px;
  padding: 40px;
  margin-right: 200px;
  flex-direction: column;
  align-items: center;
  border-radius: 20px;
  background-color: #ffffff;
`;

export const TitleContainer = styled.div`
    width: 100%;
    align-items: start;
`;

export const InputTextField = styled(TextField)`
  padding: 50px;
  border-radius: 15px;
`;
export const StyledButton = styled(Button)`
  margin-top: 24px;
  margin-bottom: 16px;
`;

export const FormContainer = styled.div`
  margin-top: 8px;

  Button {
    margin-top: 12px; /* Aumenta o espaçamento acima do botão */
    padding: 10px 16px; /* Adiciona um padding interno no botão */
    background: linear-gradient(to left, #0C888A, #3AAFB9); /* Cor de fundo do botão */
    color: white; /* Cor do texto do botão */
    border: none; /* Remove a borda do botão */
    cursor: pointer; /* Muda o cursor ao passar sobre o botão */
    border-radius: 12px; /* Adiciona um arredondamento nas bordas do botão */
  }
`;

export const StyledFormControlLabel = styled(FormControlLabel)`
  .MuiFormControlLabel-label {
    color: #BBBBBB; /* Cor da label */
  }

  .MuiCheckbox-BpIcon {
  background-color: red;
  }
  
  .MuiCheckbox-colorPrimary.Mui-checked {
    border-color: #BBBBBB;
    color: #3aafb9; /* Cor do Checkbox marcado */
  }

  .MuiSvgIcon-root {
    boxShadow: inset 0 0 0 1px rgba(16,22,26,.2);

    }
`;