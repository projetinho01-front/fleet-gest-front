import { TextField, Typography } from '@mui/material';
import BpCheckbox from './components/BpCheckbox';
import { Container, PageContainer, LoginContainer, InputTextField, StyledButton, FormContainer, TitleContainer, StyledFormControlLabel } from './Login.styles';
import logo from '../../../assets/flee-gest-logo.svg';
import { motion } from 'framer-motion';

const Login = () => {
  return (
    <PageContainer>
      <Container>
        <motion.div
            initial={{ opacity: 0, x: -300 }}
            animate={{ opacity: 1, x: 0 }}
            transition={{ duration: 1.0 }}
        >
        <img src={logo} alt="Logo" width="300" height="300" />
        </motion.div>
      </Container>


      <Container>
        <motion.div
          initial={{ opacity: 0, x: 300 }}
          animate={{ opacity: 1, x: 0 }}
          transition={{ duration: 1.0 }}
        >
          <LoginContainer>
            <TitleContainer>
              <Typography variant="h4" color={"#797979"} fontWeight={600}>
                Bem-vindo,
              </Typography>
              <Typography variant="subtitle1" color={"#A8A8A8"} fontWeight={400}>
                Por favor, entre com suas informações.
              </Typography>
            </TitleContainer>

            <FormContainer>
              <InputTextField
                margin="normal"
                required
                fullWidth
                id="usuario"
                label="Usuário"
                name="usuario"
                autoComplete="usuario"
                autoFocus
              />
              <TextField
                margin="normal"
                required
                fullWidth
                name="senha"
                label="Senha"
                type="password"
                id="senha"
                autoComplete="current-password"
              />
              <StyledButton
                type="submit"
                fullWidth
                variant="contained"
              >
                Entrar
              </StyledButton>
              <StyledFormControlLabel control={<BpCheckbox />} label="Manter conectado" />
            </FormContainer>

            <Typography variant="subtitle1" color={"#A8A8A8"} fontWeight={400}>
              Não possui conta? <a href="http://">Realizar cadastro</a>
            </Typography>
          </LoginContainer>
        </motion.div>
      </Container>
    </PageContainer>
  );
};

export default Login;
