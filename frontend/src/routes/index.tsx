import { BrowserRouter as Router, Route, Routes, useLocation } from 'react-router-dom';
import Login from '../pages/Auth/Login/Login';
import Home from '../pages/Home';
import MeusVeiculos from '../pages/MeusVeiculos/MeusVeiculos';
import MiniDrawer from '../pages/Home/components/Drawer';
import { Box } from '@mui/material';

const AppRoutes = () => {
  return (
    <Router>
      <AppContent />
    </Router>
  );
};

const AppContent = () => {
  const location = useLocation();
  const showDrawer = location.pathname !== '/login';

  return (
    <Box sx={{ display: 'flex' }}>
      {showDrawer && <MiniDrawer />}
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path="/home" element={<Home />} />
          <Route path='/meus-veiculos' element={<MeusVeiculos />} />
        </Routes>
      </Box>
    </Box>
  );
};

export default AppRoutes;
